import express from 'express';
import { Book } from './entities';
import { MissingResourceError } from './errors';
import BookService from './services/book-service';
import { BookServiceImpl } from './services/book-service-impl';

const app = express();
app.use(express.json()); // Middleware

// Our app routes should use services we create to do the heavy lifting
// try to minimize the amount of logic in your routes that is NOT related directly to HTTP requests

const bookService:BookService = new BookServiceImpl();

app.get("/books", async (req, res)=>{
        console.log("hit books");
        const books:Book[] = await bookService.retrieveAllBooks();
        res.send(books);
    
});

app.get("/books/:id", async (req, res)=>{
    try {
        console.log("hit single book");
        const bookId = Number(req.params.id);
        const book:Book = await bookService.retrieveBookById(bookId);
        res.send(book);

    } catch (error){
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
    
});

app.post("/books", async (req, res) => {
    console.log("created book");
    let book:Book = req.body;
    book = await bookService.registerBook(book);
    res.send(book);
    res.status(201);
});

app.patch("/books/:id/checkout", async (req, res) => {
    const bookId = Number(req.params.id);
    const book = await bookService.checkoutBookById(bookId);
    res.send(book);
});

app.listen(3000, ()=>{console.log("Application Started!")})