// An entity is a class that stores information that will ultimately be persisted somewhere
// Usually very little logic
// They SHOULD ALWAYS have one field in them that is a unique identifier, ID
export class Book{
    constructor(
        public bookId:number,
        public title:string,
        public author:string,
        public isAvailable:boolean,
        public quality:number,
        public returnDate:number // Typically dates are stored as unix epoch time
        // seconds from midnight January 1970.
    ){}
}