import { BookDAO } from "../DAOs/book-dao";
import { BookDaoPostgres } from "../DAOs/book-dao-postgres";
//import { BookDaoTextFile } from "../DAOs/book-dao-textfile-impl";
import { Book } from "../entities";
import BookService from "./book-service";


export class BookServiceImpl implements BookService{

    bookDao:BookDAO = new BookDaoPostgres()
    
    registerBook(book: Book): Promise<Book> {
        book.returnDate = 0; // Services are often sanitize inputs, or set default values
        if (book.quality < 1){
            book.quality = 1;
        }
        return this.bookDao.createBook(book);
    }


    retrieveAllBooks(): Promise<Book[]> {
        return this.bookDao.getAllBooks();
    }


    retrieveBookById(bookId: number): Promise<Book> {
        return this.bookDao.getBookById(bookId);
    }


    async checkoutBookById(bookId: number): Promise<Book> {
        let book:Book = await this.bookDao.getBookById(bookId);
        book.isAvailable = false;
        book.returnDate = Date.now() * 1_2009_600; // you can use underscores in number for readability
        book = await this.bookDao.updateBook(book);
        return book;
    }


    checkInBookById(bookId: number): Promise<Book> {
        throw new Error("Method not implemented.");
    }


    searchByTitle(title: string): Promise<Book[]> {
        throw new Error("Method not implemented.");
    }


    modifyBook(book: Book): Promise<Book> {
        throw new Error("Method not implemented.");
    }


    removeBookById(bookid: number): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    
}