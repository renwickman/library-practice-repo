import { Book } from "../entities";

// Your service interface should have all the methods that your RESTful web service
// will find helpful.
// 1. Methods that perform CRUD operation
// 2. Business Logic operations
export default interface BookService{

    registerBook(book:Book):Promise<Book>;

    retrieveAllBooks():Promise<Book[]>;

    retrieveBookById(bookId:number):Promise<Book>;

    checkoutBookById(bookId:number):Promise<Book>;

    checkInBookById(bookId:number):Promise<Book>;

    searchByTitle(title:string):Promise<Book[]>;

    modifyBook(book:Book):Promise<Book>;

    removeBookById(bookid:number):Promise<boolean>;

}