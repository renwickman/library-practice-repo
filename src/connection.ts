import { Client } from 'pg';
require('dotenv').config({path:'C:\\Users\\renwi\\Desktop\\Revature_Learning\\Week2\\LibraryAPI\\app.env'})

export const client = new Client({
    user: 'postgres',
    password: process.env.DBPASSWORD, // You should never store passwords in code
    database: process.env.DATABASENAME,
    port:5432,
    host: '35.223.206.29'
})
client.connect()