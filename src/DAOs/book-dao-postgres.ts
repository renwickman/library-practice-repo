import { Book } from "../entities";
import { BookDAO } from "./book-dao";
import { client } from "../connection";
import { MissingResourceError } from "../errors";

export class BookDaoPostgres implements BookDAO {

    async createBook(book: Book): Promise<Book> {
        const sql:string = "insert into book(title,author,is_available,quality,return_date) values ($1,$2,$3,$4,$5) returning book_id"
        const values = [book.title,book.author, book.isAvailable, book.quality, book.returnDate];
        const result = await client.query(sql, values);
        book.bookId = result.rows[0].book_id;
        return book; // returns an object that has the fields of book, does not return book itself
    }

    async getAllBooks(): Promise<Book[]> {
        const sql:string = "select * from book";
        const result = await client.query(sql);
        const books:Book[] = [];
        //for (const row of result.rows.map) can be done as well
        for(const row of result.rows){
            const book:Book = new Book(
                row.book_id,
                row.title,
                row.author,
                row.is_available,
                row.quality,
                row.return_date);
            books.push(book);
        }
        return books;
    }

    async getBookById(bookId: number): Promise<Book> {
        //sql query
        const sql:string = 'select * from book where book_id = $1';
        const values = [bookId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The book with id ${bookId} does not exist`);
        }
        const row = result.rows[0];
        // returns the specific object
        const book:Book = new Book(
            row.book_id,
            row.title,
            row.author,
            row.is_available,
            row.quality,
            row.return_date);
        return book;
    }

    async updateBook(book: Book): Promise<Book> {
        //sql query
        const sql:string = 'update book set title=$1, author=$2, is_available=$3, quality=$4, return_date=$5 where book_id=$6'
        //need to have values to replace the '$' signs
        const values = [book.title, book.author, book.isAvailable, book.quality, book.returnDate, book.bookId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The book with id ${book.bookId} does not exist`);
        }
        return book;
    }

    //async functions always return a Promise
    async deleteBookById(bookId: number): Promise<boolean> {
        const sql:string = 'delete from book where book_id = $1'
        const values = [bookId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The book with id ${bookId} does not exist`);
        }

        return true;
    }

}